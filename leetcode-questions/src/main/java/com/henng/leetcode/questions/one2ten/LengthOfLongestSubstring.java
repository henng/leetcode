package com.henng.leetcode.questions.one2ten;

import org.junit.Test;

/**
 * LengthOfLongestSubstring
 *
 * @author henng
 * @since 10/14/16
 */

/**
 * Examples:

 Given "abcabcbb", the answer is "abc", which the length is 3.

 Given "bbbbb", the answer is "b", with the length of 1.

 Given "pwwkew", the answer is "wke", with the length of 3.
 Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

public class LengthOfLongestSubstring {

    public int lengthOfLongestSubstring(String s) {
        assert (null != s);
        // should we remove all of space(s)?
        //s = s.replaceAll(" ", "");

        int maxLength = 0;
        //String maxLengthString = null;

        int length = s.length();
        if (length == 0 || length == 1) {
            //System.out.println("the longest subString is : " + s);
            return length;
        }

        for (int i = 0; i < length; i++) {
            String sub = s.charAt(i) + "";
            for (int j = i + 1; j < length; j++) {
                char next = s.charAt(j);
                if (sub.indexOf(next) != -1) {
                    break;
                } else {
                    sub = s.substring(i, j + 1);
                }

            }
            int subLength = sub.length();
            if (maxLength < subLength) {
                maxLength = subLength;
                //maxLengthString = sub;
            }
        }

        //System.out.println("the longest subString is : " + maxLengthString);
        return maxLength;
    }

    @Test
    public void lengthOfLongestSubstringTest() {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
        System.out.println(lengthOfLongestSubstring("au"));
        System.out.println(lengthOfLongestSubstring("pwwkew"));
    }
}
