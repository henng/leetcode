package com.henng.leetcode.questions.one2ten;

/**
 * ReverseInteger
 *
 * @author henng
 * @since 4/1/17
 */

import org.junit.Test;

/**
 * Description
 *
 * Reverse digits of an integer.
 Example1: x = 123, return 321
 Example2: x = -123, return -321

 Note:
 The input is assumed to be a 32-bit signed integer. Your function should return 0 when the reversed integer overflows.

 */
public class ReverseInteger {

    public int reverse(int x) {
        StringBuffer buffer = new StringBuffer();
        int end = 0;
        if (x < 0) {
            end = 1;
        }
        String xx = x + "";
        for (int i = xx.length() - 1; i >= end; i--) {
            buffer.append(xx.charAt(i));
        }
        long result = Long.parseLong(buffer.toString());
        if (x < 0) {
            result = 0 - result;
        }
        if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) {
            result = 0;
        }

        return (int) result;
    }

    @Test
    public void ReverseIntegerTest() {
        System.out.println(reverse(-100003));
    }


}
