package com.henng.leetcode.questions.one2ten;

import org.junit.Test;

/**
 * LongestPalindrome
 *
 * @author henng
 * @since 1/6/17
 */

/**
 * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

 Example:
 Input: "babad"
 Output: "bab"
 Note: "aba" is also a valid answer.

 Example:
 Input: "cbbd"
 Output: "bb"

 Especial test cases:
 Input: "aaaa"
 Input: "tattarrattat"
 Input: "aaa..." (num of a are 1000)
 */

public class LongestPalindrome {

    public String longestPalindrome(String s) {
        assert (null != s && !s.isEmpty());

        int length = s.length();
        if (length == 1) {
            return s;
        }

        String output = "";
        for (int i = 0; i < length - 1; i++) {

            String outputOdd  = getOutput(s, i, i);
            String outputEven = getOutput(s, i, i + 1);

            String localMax = outputOdd.length() > outputEven.length() ? outputOdd : outputEven;
            if (localMax.length() > output.length()) {
                output = localMax;
            }
        }

        return output;
    }

    private String getOutput(final String s, int prev, int next) {
        if (prev != next) {
            if (s.charAt(prev) == s.charAt(next)) {
                while (prev > 0 && next < s.length() - 1 && s.charAt(prev - 1) == s.charAt(next + 1)) {
                    prev = prev - 1;
                    next = next + 1;
                }
            } else {
                return "";
            }
        } else {
            while (prev > 0 && next < s.length() - 1 && s.charAt(prev - 1) == s.charAt(next + 1)) {
                prev = prev - 1;
                next = next + 1;
            }
        }

        return s.substring(prev, next + 1);
    }

    @Test
    public void longestPalindromeTest() {
        String input = "tattarrattat";
        String output = longestPalindrome(input);
        System.out.println(output);
    }

}
