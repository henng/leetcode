package com.henng.leetcode.questions.one2ten;

import org.junit.Test;

/**
 * AddTwoNumbers
 *
 * @author henng
 * @since 10/11/16
 */

/**
 * Description

 You are given two linked lists representing two non-negative numbers.
 The digits are stored in reverse order and each of their nodes contain a single digit.
 Add the two numbers and return it as a linked list.

 Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 Output: 7 -> 0 -> 8
 *
 */

public class AddTwoNumbers {

    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
        }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        assert (l1 != null && l2 != null);
        assert (l1.val >= 0 && l2.val >= 0);

        int currSum = l1.val + l2.val;

        ListNode head = new ListNode(currSum >= 10? currSum - 10 : currSum);
        ListNode result = head;
        ListNode next1 = l1.next;
        ListNode next2 = l2.next;

        while (null != next1 || null != next2) {

            int nextSum = 0;
            if (null != next1) {
                nextSum += next1.val;
                next1 = next1.next;
            }
            if (null != next2) {
                nextSum += next2.val;
                next2 = next2.next;
            }

            if (currSum >= 10) {
                nextSum += 1;
            }
            result.next = new ListNode(nextSum >= 10? nextSum - 10 : nextSum);
            result = result.next;
            currSum = nextSum;

        }

        // the last sum is still over than 10
        if (currSum >= 10) {
            result.next = new ListNode(1);
            result = result.next;
        }

        result.next = null;
        return head;
    }

    /*public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;

        ListNode newHead = new ListNode(0);
        ListNode p1 = l1, p2 = l2, p3 = newHead;

        while (p1 != null || p2 != null) {
            if (p1 != null) {
                carry += p1.val;
                p1 = p1.next;
            }

            if (p2 != null) {
                carry += p2.val;
                p2 = p2.next;
            }

            p3.next = new ListNode(carry%10);
            p3 = p3.next;
            carry /= 10;
        }

        if(carry == 1)
            p3.next = new ListNode(1);

        return newHead.next;
    }*/

    @Test
    public void testAddTwoNumbers() {
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        //l1.next.next = new ListNode(3);
        //l1.next.next.next = new ListNode(8);
        l1.next.next = null;

        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);
        l2.next.next.next = new ListNode(3);
        l2.next.next.next.next = null;

        ListNode result = addTwoNumbers(l1, l2);

        while (null != result) {
            System.out.println(result.val);
            result = result.next;
        }
    }

}
