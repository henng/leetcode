package com.henng.leetcode.questions.one2ten;

import org.junit.Test;

/**
 * PalindromeNumber
 *
 * @author henng
 * @since 4/16/17
 */
public class PalindromeNumber {

    public boolean isPalindrome(int x) {
        if (x == 0) {
            return true;
        }
        if (x < 0 || x % 10 == 0) {
            return false;
        }

        int digits = 0;
        int temp = x;
        while (temp > 0) {
            temp = temp / 10;
            digits++;
        }

        // in case that reverse is overflow, we use long here
        long reverse = 0;
        temp = x;
        while (temp > 0) {
            int unit = temp % 10;
            reverse += unit * Math.pow(10, --digits);
            temp = temp / 10;
        }

        return x == reverse;
    }

    @Test
    public void PalindromeTest() {
        // 2147483647
        int x = 1;
        System.out.println(isPalindrome(x));
    }

}
