package com.henng.leetcode.questions.one2ten;

import org.junit.Test;

/**
 * FindMedianSortedArrays
 *
 * @author henng
 * @since 10/17/16
 */

/**
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.

 Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

 Example 1:
 <code>
 nums1 = [1, 3]
 nums2 = [2]
 </code>
 The median is 2.0

 Example 2:
 <code>
 nums1 = [1, 2]
 nums2 = [3, 4]
 </code>
 The median is (2 + 3)/2 = 2.5

 Finally we make it to this problem : find the k-th num of the two sorted array
 */

public class FindMedianSortedArrays {

    /*public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        if (null == nums1 || nums1.length == 0) {
            int len = nums2.length;
            return len % 2 == 0? (nums2[len/2] + nums2[len/2-1]) / 2.0 : nums2[len/2];
        } else if (null == nums2 || nums2.length == 0) {
            int len = nums1.length;
            return len % 2 == 0? (nums1[len/2] + nums1[len/2-1]) / 2.0 : nums1[len/2];
        }

        int len1 = nums1.length;
        int len2 = nums2.length;
        if (len1 > len2) {
            return findMedianSortedArrays(nums2, nums1);
        }

        int splitter = (len1 + len2) >> 1;
        int lowBound = 0;
        int upBound = len1;
        int i = 0, j = 0, c1 = 0, c2 = 0;

        while (lowBound <= upBound) {
            i = (lowBound + upBound) / 2;
            j = splitter - i;
            if (i > 0 && j < len2 && nums1[i-1] > nums2[j]) {
                upBound = i - 1;
            } else if (j > 0 && i < len1 && nums1[i] < nums2[j-1]) {
                lowBound = i + 1;
            } else {
                break;
            }
        }

        if (i == len1) {
            c1 = nums2[j];
        } else if (j == len2) {
            c1 = nums1[i];
        } else {
            c1 = Math.min(nums1[i], nums2[j]);
        }

        if ((len1 + len2) % 2 != 0) {
            return c1;
        }

        if (i == 0) {
            c2 = nums2[j-1];
        } else if (j == 0) {
            c2 = nums1[i-1];
        } else {
            c2 = Math.max(nums1[i-1], nums2[j-1]);
        }

        return (c1 + c2) / 2.0;

    }*/


    public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        if (null == nums1 && null == nums2) {
            return -1.0;
        } else if (null == nums1 || nums1.length == 0) {
            int len = nums2.length;
            return len % 2 == 0? (nums2[len>>1] + nums2[(len>>1)-1]) / 2.0 : nums2[len>>1];
        } else if (null == nums2 || nums2.length == 0) {
            int len = nums1.length;
            return len % 2 == 0? (nums1[len>>1] + nums1[(len>>1)-1]) / 2.0 : nums1[len>>1];
        }

        int len1 = nums1.length;
        int len2 = nums2.length;
        int k = 1;
        if ((len1 + len2)%2 == 0) {
            return (findKth(nums1, nums2, 0, 0, len1, len2, k) + findKth(nums1, nums2, 0, 0, len1, len2, k+1)) / 2;
        }   else {
            return findKth(nums1, nums2, 0, 0, len1, len2, k+1);
        }
    }

    private double findKth(int[] arr1, int[] arr2, int start1, int start2, int len1, int len2, int k) {
        if (len1 > len2) {
            return findKth(arr2, arr1, start2, start1, len2, len1, k);
        }

        if (len1 == 0) {
            return arr2[start2 + k - 1];
        }

        if (k == 1) {
            return Math.min(arr1[start1], arr2[start2]);
        }

        int p1 = Math.min(k>>1, len1);
        //int p1 = (len1 + 1) / 2;
        int p2 = k - p1;

        if (arr1[start1 + p1-1] < arr2[start2 + p2-1]) {
            return findKth(arr1, arr2, start1 + p1, start2, len1-p1, len2, k-p1);
        } else if (arr1[start1 + p1-1] > arr2[start2 + p2-1]) {
            return findKth(arr1, arr2, start1, start2 + p2, len1, len2-p2, k-p2);
        } else {
            return arr1[start1 + p1-1];
        }
    }

    @Test
    public void findMedianSortedArraysTest() {
        int[] nums1 = {1};
        int[] nums2 = {2, 3};
        double median = findMedianSortedArrays(nums1, nums2);
        System.out.println(median);
    }

}
