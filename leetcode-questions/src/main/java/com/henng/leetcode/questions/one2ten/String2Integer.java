package com.henng.leetcode.questions.one2ten;

/**
 * String2Integer
 *
 * @author henng
 * @since 4/1/17
 */

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Description
 *
 * Implement atoi to convert a string to an integer.
 * @see Integer#parseInt(String) in Java
 * NOTICE: THEY TWO ABOVE ARE NOT THE SAME

 Hint: Carefully consider all possible input cases. If you want a challenge, please do not see below and ask yourself what are the possible input cases.

 Notes: It is intended for this problem to be specified vaguely (ie, no given input specs). You are responsible to gather all the input requirements up front.
 */
public class String2Integer {

    public int myAtoi(String str) {
        if (null == str || str.isEmpty()) {
            return 0;
        }

        /*Pattern pattern = Pattern.compile(".*\\d+.*");
        Matcher matcher = pattern.matcher(str);
        if (!matcher.matches()) {
            return 0;
        }*/

        int INT_MAX = 2147483647;
        int INT_MIN = (-INT_MAX - 1);

        int length = str.length();
        int begin = -1;
        int end = -1;
        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            if (c != ' ' && !isLegalChar(c) && begin == -1) {
                return 0;
            }
            if (isLegalChar(c) && begin == -1) {
                begin = i;
                if (i == length - 1) {
                    end = i;
                }
                continue;
            }
            if ((!isDigit(c) && begin != -1) || i == length - 1) {
                end = i;
                break;
            }
        }

        if (begin == -1 || end == -1) {
            return 0;
        }

        char first = str.charAt(begin);
        char stop  = str.charAt(end);
        if (first == stop && !isDigit(first)) {
            return 0;
        }

        if (first == '-' || first == '+') {
            begin++;
        }
        if (!isDigit(stop)) {
            end--;
        }
        str = str.substring(begin, end + 1);

        long result = 0;
        length = str.length();
        for (int i = length - 1; i >= 0; i--) {
            char c = str.charAt(i);
            int digit = getIntegerByChar(c);
            result += digit * Math.pow(10, length - i - 1);
        }

        if (first == '-') {
            result = 0 - result;
            if (result < INT_MIN) {
                result = INT_MIN;
            }
        } else {
            if (result > INT_MAX) {
                result = INT_MAX;
            }
        }

        return (int) result;
    }

    private int getIntegerByChar(char c) {
        switch (c) {
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            default:
                return -1;
        }
    }

    private boolean isDigit(char c) {
        return c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5'
                || c == '6' || c == '7' || c == '8' || c == '9';
    }

    private boolean isLegalChar(char c) {
        return isDigit(c) || c == '-' || c == '+';
    }

    @Test
    public void String2IntegerTest() {
        /**
         * test cases:
         * "+"
         * "   -04f"
         * " b11228552307"
         * "214748364ss"
         * "-2147483648"
         */
        String str = "123456.78";
        System.out.println(myAtoi(str));
    }

}
