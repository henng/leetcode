package com.henng.leetcode.questions.one2ten;

/**
 * com.henng.leetcode.questions.one2ten
 *
 * @author henng
 * @since 6/25/17
 */

import org.junit.Test;

/**
 * Description
 * Write a function to find the longest common prefix string amongst an array of strings.
 */
public class LongestCommonPrefix {

    public String longestCommonPrefix(String[] strs) {
        if (null == strs) {
            return null;
        }
        if (strs.length == 0) {
            return "";
        }
        String minStr = strs[0];
        int minLen = minStr == null ? 0 : minStr.length();
        for (String s : strs) {
            if (null == s) {
                return null;
            }
            if (s.isEmpty()) {
                return "";
            }
            int len = s.length();
            if (len < minLen) {
                minLen = len;
                minStr = s;
            }
        }

        return findCommonStr(strs, minStr);
    }

    private String findCommonStr(String[] strs, String minStr) {
        for (String s : strs) {
            if (!s.startsWith(minStr)) {
                int tempLen = minStr.length();
                if (tempLen <= 1) {
                    return "";
                }
                minStr = minStr.substring(0, tempLen - 1);
                return findCommonStr(strs, minStr);
            }
        }
        return minStr;
    }

    @Test
    public void longestCommonPrefixTest() {
        //String[] strs = {"a", "b", "c"};
        String[] strs = {null, "a b", "abc"};
        System.out.println(longestCommonPrefix(strs));
    }

}
