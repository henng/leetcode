package com.henng.leetcode.questions.one2ten;

import org.junit.Test;

/**
 * TwoSum
 *
 * @author henng
 * @since 10/11/16
 */

/** Description

 Given an array of integers, return indices of the two numbers such that they add up to a specific target.

 You may assume that each input would have exactly one solution.

 Example:
 Given nums = [2, 7, 11, 15], target = 9,

 Because nums[0] + nums[1] = 2 + 7 = 9,
 return [0, 1].
 */

public class TwoSum {

    @Test
    public void testTwoSum() {
        int[] nums = {2, 7, 11, 15};
        int target = 22;
        int[] result = twoSum(nums, target);
        System.out.println(result[0] + ", " + result[1]);
    }

    public int[] twoSum(int[] nums, int target) {
        if (nums.length <= 0) {
            throw new IllegalArgumentException("No element in array of nums.");
        }

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    int[] result = new int[2];
                    result[0] = i;
                    result[1] = j;
                    return result;
                }
            }
        }

        return null;
    }

}
