package com.henng.leetcode.questions.one2ten;

/**
 * ZigZagConversion
 *
 * @author henng
 * @since 4/1/17
 */

import org.junit.Test;

/**
 * Description
 *
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
 * (you may want to display this pattern in a fixed font for better legibility)

 P   A   H   N
 A P L S I I G
 Y   I   R
 And then read line by line: "PAHNAPLSIIGYIR"
 Write the code that will take a string and make this conversion given a number of rows:

 string convert(string text, int nRows);
 convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
 */

public class ZigZagConversion {

    private String convert(String text, int nRows) {

        if (text == null) {
            return null;
        }
        if (nRows == 1) {
            return text;
        }

        int textLength = text.length();
        int nColumn;
        if (textLength <= nRows) {
            nColumn = 1;
        } else if (textLength <= (2 * nRows - 2) ) {
            nColumn = 1 + textLength - nRows;
        } else {
            int quotient = textLength / (2 * nRows -2);
            int remainder = textLength % (2 * nRows -2);
            if (remainder <= nRows) {
                nColumn = (nRows - 1) * quotient + 1;
            } else {
                nColumn = (nRows - 1) * quotient + 1 + remainder - nRows;
            }
        }

        char[][] letters = new char[nRows][nColumn];
        int rowIndex = 0;
        int columnIndex = 0;

        for (int textIndex = 0; textIndex < textLength; ) {
            while (rowIndex < nRows && textIndex < textLength) {
                letters[rowIndex][columnIndex] = text.charAt(textIndex);
                rowIndex++;
                textIndex++;
            }
            --rowIndex;
            while (rowIndex > 0 && textIndex < textLength) {
                letters[--rowIndex][++columnIndex] = text.charAt(textIndex);
                textIndex++;
            }
            ++rowIndex;
        }

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < letters.length; i++) {
            for (int j = 0; j < letters[i].length; j++) {
                if (letters[i][j] != '\u0000') {
                    buffer.append(letters[i][j]);
                }
            }
        }

        return buffer.toString();
    }

    @Test
    public void ZigZagTest() {
        String text = "PAYPALISHIRING";
        System.out.println("length : " + text.length());
        int nRows = 5;
        System.out.println(convert(text, nRows));
    }
}
