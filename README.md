# leetcode

[![Gitlab CI](https://gitlab.com/henng/leetcode/badges/master/build.svg)](https://github.com/henng/leetcode)
[![license](https://img.shields.io/github/license/henng/leetcode.svg)](https://github.com/henng/leetcode/blob/master/LICENSE)

Practices For Leetcode (https://leetcode.com/problemset/algorithms/)
